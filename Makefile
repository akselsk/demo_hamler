.PHONY : build run test repl

all: build f

f:
	@erlc -o ebin src/*.erl	

build:
	@hamler build

run:
	@hamler run
test:
	@hamler test
repl:
	@hamler repl
